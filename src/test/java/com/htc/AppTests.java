package com.htc;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.model.Person;
import com.htc.service.PersonService;


@RunWith(SpringRunner.class)
@SpringBootTest
public class AppTests {
	
    private static final Logger log = LoggerFactory.getLogger(AppTests.class);
    
    @Autowired private PersonService personService;
    
    @BeforeClass
    public static void beforeClass() {
    	
    	log.info("Info");
    	log.debug("Debug");
    	log.warn("Warning");
    	log.error("Error");
    	
    }
    
    @Before
    public void setUpTest() {
        log.info("Before");
    }
    
    @Test
    public void test1() {
 		
    	
    	
    	//personService.getPersonNameById(-1).length();
    	
    	Person person = new Person();
    	
    	personService.insert(person);
		
       // assertNotNull(personService.getPersonByName("wsdl"));
    }
    

    @After
    public void tearDownTest() {
        log.info("After ");
    }
    
    @AfterClass
    public static void tearDownClass() {
        log.info("After");
    }

}

