-- Database: test

-- DROP DATABASE test;

CREATE DATABASE test
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Spanish_Spain.1252'
    LC_CTYPE = 'Spanish_Spain.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

create table person(
	id 		serial primary key,
	name 	varchar(30) not null,
	age    integer not null
);

insert into person ( name, age) values
('Juan', 21),
('Jose', 30),
('Pedro', 32);

select * from person;


