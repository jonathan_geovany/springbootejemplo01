package com.htc.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.htc.model.Person;


@Repository
public class PersonDaoImpl extends JdbcDaoSupport implements PersonDao {
	
	@Autowired
	DataSource dataSource;
	
	@PostConstruct
	public void initalize() {
		setDataSource(dataSource);
	}
	
	public PersonDaoImpl() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int insert(Person p) {
		String sql = "insert into "+Person.TABLE+"(id,name,age) values (?,?,?)";
		return getJdbcTemplate().update(sql,new Object[] {p.getId(),p.getName(),p.getAge()});
	}

	@Override
	public int[] insertList(List<Person> list) {
		String sql = "insert into "+Person.TABLE+"(id,name,age) values (?,?,?)";
		return getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement stmt, int index) throws SQLException {
				// TODO Auto-generated method stub
				Person person = list.get(index);
				stmt.setInt(1, person.getId());
				stmt.setString(2, person.getName());
				stmt.setInt(3, person.getAge());
			}
			@Override
			public int getBatchSize() {
				// TODO Auto-generated method stub
				return list.size();
			}
		});
		
	}

	@Override
	public List<Person> getAllPersons() {
		String sql = "select * from "+Person.TABLE;
		List<Map<String, Object >> queryResult = getJdbcTemplate().queryForList(sql);
		List<Person> list = new ArrayList<>();
		for (Map<String, Object > r : queryResult) {
			Person person = new Person();
			person.setId( (Integer) r.get("id")  );
			person.setName( (String) r.get("name") );
			person.setAge((Integer) r.get("age"));
			list.add(person);
		}
		return list;
	}

	@Override
	public Person getPersonById(Integer id) {
		String sql = "select * from "+Person.TABLE+" where id=?";
		return getPersonByParameter(sql, id);
	}
	
	@Override
	public Person getPersonByName(String name) {
		String sql = "select * from "+Person.TABLE+" where name=?";
		return getPersonByParameter(sql, name);
	}
	

	@Override
	public int getPersonsCount() {
		String sql = "select count(*) from "+Person.TABLE;
		return getJdbcTemplate().queryForObject(sql, Integer.class);
	}

	@Override
	public String getPersonNameById(Integer id) {
		String sql = "select name from "+Person.TABLE+" where id=?";
		return getJdbcTemplate().queryForObject(sql,new Object[] {id},String.class);
	}
	
	private Person getPersonByParameter(String sql,Object param) {
		return (Person)getJdbcTemplate().queryForObject(sql, new Object[] {param},new RowMapper<Person>() {
			@Override
			public Person mapRow(ResultSet r, int arg1) throws SQLException {
				Person person = new Person();
				person.setId( r.getInt("id")  );
				person.setName(r.getString("name") );
				person.setAge(r.getInt("age"));
				return person;
			}
		});
	}

	@Override
	public int delete(Person p) {
		// TODO Auto-generated method stub
		String sql = "delete from "+Person.TABLE+" where id=?";
		return getJdbcTemplate().update(sql,new Object[] {p.getId()});
	}

}
