package com.htc.service;

import java.util.List;


import com.htc.model.Person;

public interface PersonService {
	public int insert(Person p);
	public int[] insertList(List<Person> list);
	public int delete(Person p);
	public List<Person> getAllPersons();
	public Person getPersonById(Integer id);
	public Person getPersonByName(String name);
	public String getPersonNameById(Integer id);
	public int getPersonsCount();
}
