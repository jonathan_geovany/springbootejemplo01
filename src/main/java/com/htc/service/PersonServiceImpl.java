package com.htc.service;

import java.security.InvalidParameterException;
import java.util.EmptyStackException;
import java.util.List;

import org.postgresql.util.PSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.htc.AppTests;
import com.htc.dao.PersonDao;
import com.htc.model.Person;

@Component
public class PersonServiceImpl implements PersonService {

    private static final Logger log = LoggerFactory.getLogger(PersonServiceImpl.class);
	
	@Autowired PersonDao personDao;

	@Override
	public int insert(Person p) {
		int id;
		try {
			return personDao.insert(p);
		} catch (DuplicateKeyException e) {
			// TODO: handle exception
			log.error("Excepcion en funcion [ insert ] detalles : llave duplicada "+p.getId());
		} catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			log.error("Excepcion en funcion [ insert ] detalles : "+e.getMessage());
		} catch (Exception e) {
			// TODO: handle exception
			log.error("Excepcion en funcion [ insert ] detalles : "+e.getMessage());
		}
		return -1;
	}

	@Override
	public int[] insertList(List<Person> list) {
		try {
			return personDao.insertList(list);
		} catch (DuplicateKeyException e) {
			// TODO: handle exception
			log.error("Excepcion en funcion [ insertList ] detalles : llaves duplicadas");
		} catch (DataIntegrityViolationException e) {
			// TODO: handle exception
			log.error("Excepcion en funcion [ insert ] detalles : "+e.getMessage());
		} catch(Exception e) {
			log.error("Excepcion en funcion [ insertList ] detalles : "+e.getMessage());
		}
		return null;
	}

	@Override
	public List<Person> getAllPersons() {
		try {
			return personDao.getAllPersons();
		} catch (Exception e) {
			log.error("Excepcion en funcion [ getAllPersons ] detalles : "+e.getMessage());
			return null;
		}
	}

	@Override
	public Person getPersonById(Integer id) {
		try {
			return personDao.getPersonById(id);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
			log.error("Excepcion en funcion [ getPersonById ] detalles : no hay persona con el id "+id);
		} catch (Exception e) {
			// TODO: handle exception
			log.error("Excepcion en funcion [ getPersonById ] detalles : "+e.getMessage());
		}
		return null;
	}

	@Override
	public Person getPersonByName(String name) {
		try {
			return personDao.getPersonByName(name);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
			log.error("Excepcion en funcion [ getPersonByName ] detalles : no hay persona con el nombre "+name);
		} catch (Exception e) {
			// TODO: handle exception
			log.error("Excepcion en funcion [ getPersonByName ] detalles : "+e.getMessage());
		}
		return null;
	}

	@Override
	public String getPersonNameById(Integer id) {
		try {
			return personDao.getPersonNameById(id);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
			log.error("Excepcion en funcion [ getPersonById ] detalles : no hay datos para el id "+id);
		} catch (Exception e) {
			// TODO: handle exception
			log.error("Excepcion en funcion [ getPersonById ] detalles : "+e.getMessage());
		}
		return null;
	}

	@Override
	public int getPersonsCount() {
		try {
			return personDao.getPersonsCount();
		} catch (Exception e) {
			log.error("Excepcion en funcion [ getPersonsCount"
					+ " ] detalles : "+e.getMessage());
			return -1;
		}
	}

	@Override
	public int delete(Person p) {
		
		
		try {
			if(p==null) {
				throw new NullPointerException();
			}
			if(p.getId()==-1) {
				throw new InvalidParameterException();
			}
			return personDao.delete(p);
		} catch (NullPointerException e) {
			// TODO: handle exception
			log.error("Excepcion en funcion [ getPersonById ] detalles : clase null enviada");
		} catch (InvalidParameterException e) {
			// TODO: handle exception
			log.error("Excepcion en funcion [ getPersonById ] detalles : id no se ha establecido");
		} catch (Exception e) {
			// TODO: handle exception
			log.error("Excepcion en funcion [ getPersonById ] detalles : "+e.getMessage());
		}
		return -1;
	}

}
