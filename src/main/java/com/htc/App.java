package com.htc;

import java.util.ArrayList;

import org.postgresql.util.PSQLException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.dao.DuplicateKeyException;

import com.htc.model.Person;
import com.htc.service.PersonService;


@SpringBootApplication
@ComponentScan("com.htc.service, com.htc.dao")
public class App {
	
	
	public static void main(String[] args) {
		 ApplicationContext ctx = SpringApplication.run(App.class, args);
		 PersonService personService = ctx.getBean(PersonService.class);
		
		
		ArrayList<Person> persons = new ArrayList<>();
		persons.add(new Person(1,"Manuel",24));
		persons.add(new Person(2,"Pedro",20));
		persons.add(new Person(3,"Luis",25));
		persons.add(new Person(4,"Maria",26));
		persons.add(new Person(5,"Flor",30));
		
		//personService.getPersonNameById(99);
		//personService.insert(new Person());
		System.out.println("Eliminando "+personService.delete(new Person()));
		
		 //System.out.println("Elementos en la BD: "+personService.getPersonsCount());
		
	}
	
	
	

}

